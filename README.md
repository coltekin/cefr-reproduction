This repository contains scripts for reproducing

> Vajjala, S. and Rama, T. (2018). Experiments with Universal CEFR Classification. In Proceedings of the Thirteenth Workshop on Innovative Use of NLP for Building Educational Applications, pages 147–153.

within the context of REPROLANG 2020.

Most of the code is located in `image/cefr-scoring/`, which is
automatically copied to the docker image. This is an almost
identical copy of the code by the authors of the above paper.
The original paper is available at
<https://github.com/nishkalavallabhi/UniversalCEFRScoring>.

The code is modified minimally to print out scores in a distinctive
way so that a wrapper script can easily extract the results presented
in the tables to be reproduced. We also removed the fixed random seed
in the original code to be able to quantify the variation in the results.

The only additional script `image/make-tables.py`
extracts the scores from the output of the software,
and prints them as formatted tables.
