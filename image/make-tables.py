#!/usr/bin/env python3

import sys, os, argparse, re
import numpy as np

vr_tbl2 = {
    'base': {'de': 0.479, 'it': 0.578, 'cz': 0.587},
    'word': {'de': 0.666, 'it': 0.827, 'cz': 0.721},
    'pos': {'de': 0.663, 'it': 0.825, 'cz': 0.699},
    'dep': {'de': 0.663, 'it': 0.813, 'cz': 0.704},
    'dom': {'de': 0.533, 'it': 0.653, 'cz': 0.663},
    'word+dom': {'de': 0.686, 'it': 0.837, 'cz': 0.734},
    'pos+dom': {'de': 0.686, 'it': 0.816, 'cz': 0.709},
    'dep+dom': {'de': 0.682, 'it': 0.806, 'cz': 0.712},
    'emb': {'de': 0.646, 'it': 0.794, 'cz': 0.625}
}

vr_tbl3 = {
    'base': {'-': 0.428, '+': None},
    'word': {'-': 0.721, '+': 0.719},
    'pos':  {'-': 0.726, '+': 0.724},
    'dep':  {'-': 0.703, '+': 0.693},
    'dom':  {'-': 0.449, '+': 0.471},
    'emb':  {'-': 0.693, '+': 0.689}
} 

vr_tbl4 = {
    'base': {'it': 0.553, 'cz': 0.487},
    'pos':  {'it': 0.758, 'cz': 0.649},
    'dep':  {'it': 0.624, 'cz': 0.653},
    'dom':  {'it': 0.63, 'cz': 0.475}
}

argp = argparse.ArgumentParser()
argp.add_argument('input', nargs="+")
argp.add_argument('--latex', '-l', action='store_true')
argp.add_argument('--output-dir', '-o')
args = argp.parse_args()

mono = {
    'base': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'word': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'pos': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'dep': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'dom': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'word+dom': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'pos+dom': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'dep+dom': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
    'emb': {'de': {'w':[], 'm':[]}, 'it': {'w':[], 'm':[]},
        'cz': {'w':[], 'm':[]}},
}

multi = {
        'base': {'-': {'w':[], 'm':[]}, '+': {'w':[], 'm':[]}},
        'word': {'-': {'w':[], 'm':[]}, '+': {'w':[], 'm':[]}},
        'pos':  {'-': {'w':[], 'm':[]}, '+': {'w':[], 'm':[]}},
        'dep':  {'-': {'w':[], 'm':[]}, '+': {'w':[], 'm':[]}},
        'dom':  {'-': {'w':[], 'm':[]}, '+': {'w':[], 'm':[]}},
        'emb':  {'-': {'w':[], 'm':[]}, '+': {'w':[], 'm':[]}}
}
cross = {
        'base': {'it': {'w':[], 'm':[]}, 'cz': {'w':[], 'm':[]}},
        'pos':  {'it': {'w':[], 'm':[]}, 'cz': {'w':[], 'm':[]}},
        'dep':  {'it': {'w':[], 'm':[]}, 'cz': {'w':[], 'm':[]}},
        'dom':  {'it': {'w':[], 'm':[]}, 'cz': {'w':[], 'm':[]}}
}

for f in args.input:
    with open(f, 'r') as fp:
        for line in fp:
            if not line.startswith('RESULT'): continue
            fields = line.strip().split()[1:]
            if fields[0] == 'cross':
                tgt, fset, f1w, f1m, clf = fields[1:]
                tgt = tgt.replace('de->', '')
                if fset == 'domain': fset = 'dom'
                cross[fset][tgt]['m'].append(float(f1m))
                cross[fset][tgt]['w'].append(float(f1w))
            elif fields[0].startswith('mega'):
                fset, f1w, f1m, clf = fields[1:]
                if fset == 'domain': fset = 'dom'
                lfeat = fields[0][-1]
                multi[fset][lfeat]['m'].append(float(f1m))
                multi[fset][lfeat]['w'].append(float(f1w))
            elif fields[0] == 'base':
                lang, f1w, f1m, clf = fields[1:]
                if lang == 'mega':
                    multi['base']['-']['w'].append(float(f1w))
                    multi['base']['-']['m'].append(float(f1m))
                elif 'de->' in lang:
                    tgt = lang.replace('de->', '')
                    cross['base'][tgt]['w'].append(float(f1w))
                    cross['base'][tgt]['m'].append(float(f1m))
                else:
                    mono['base'][lang]['w'].append(float(f1w))
                    mono['base'][lang]['m'].append(float(f1m))
            elif fields[0] == 'embedings':
                lang, f1w, f1m = fields[1:]
                mono['emb'][lang]['w'].append(float(f1w))
                mono['emb'][lang]['m'].append(float(f1m))
            elif 'multiling' in fields[0]:
                f1w, f1m = fields[1:]
                if 'nolang' in fields[0]:
                    multi['emb']['-']['w'].append(float(f1w))
                    multi['emb']['-']['m'].append(float(f1m))
                else:
                    multi['emb']['+']['w'].append(float(f1w))
                    multi['emb']['+']['m'].append(float(f1m))
            else:
                fset, lang, f1w, f1m, clf = fields
                if fset == 'domain': fset = 'dom'
                mono[fset][lang]['m'].append(float(f1m))
                mono[fset][lang]['w'].append(float(f1w))

pm, sep, eol =  "±", "", ""
if args.latex:
    pm, sep, eol =  "+-", "& ", "\\\\"

if args.output_dir:
    fp = open(os.path.join(args.output_dir, "table2.txt"), "wt")
else:
    fp = sys.stdout

print("%\n% Table 2 in the original article\n%", file=fp)
if not args.latex:
    print("Featureset F1w(de) σF1w(de) ∆F1w(de) F1m(de) σF1m(de) "
          "F1w(it) σF1w(it) ∆F1w(it) F1m(it) σF1m(it) "
          "F1w(cz) σF1w(cz) ∆F1w(cz) F1m(cz) σF1m(cz)", file=fp)
for k,v in mono.items():
    print("{:10}".format(k), end="", file=fp)
    for lang in ("de", "it", "cz"):
        if not v[lang]['w']:
            print("      ", end=sep, file=fp)
            continue
        print("{}{:2.1f} {}{}{:2.2f} {}{:+2.1f} {}{:2.1f} {}{}{:2.1f}".format(
            sep, np.mean(v[lang]['w']),
            sep, pm, np.std(v[lang]['w']),
            sep, np.mean(v[lang]['w']) - 100*vr_tbl2[k][lang],
            sep, np.mean(v[lang]['m']),
            sep, pm, np.std(v[lang]['m'])), end=" ", file=fp)
    print(eol, file=fp)

if args.output_dir:
    fp.close()
    fp = open(os.path.join(args.output_dir, "table3.txt"), "wt")
else:
    fp = sys.stdout

print("%\n% Table 3 in the original article\n%", file=fp)
if not args.latex:
    print("Featureset F1w(-) σF1w(-) ∆F1w(-) F1m(-) σF1m(-) "
          "F1w(-) σF1w(-) ∆F1w(-) F1m(-) σF1m(-)", file=fp)
for k,v in multi.items():
    print("{:10}".format(k), end="", file=fp)
    for type_ in ("-", "+"):
        if not v[type_]['w'] or (type_ == '+' and k == 'base'):
            print("{0}{{-}}{0}{{-}}{0}{{-}}{0}{{-}}{0}{{-}}".format(sep),
                    end="", file=fp)
            continue
        print("{}{:2.1f} {}{}{:2.2f} {}{:+2.1f} {}{:2.1f} {}{}{:2.1f}".format(
            sep, np.mean(v[type_]['w']),
            sep, pm, np.std(v[type_]['w']),
            sep, np.mean(v[type_]['w']) - 100*vr_tbl3[k][type_],
            sep, np.mean(v[type_]['m']),
            sep, pm, np.std(v[type_]['m'])), end=" ", file=fp)
    print(eol, file=fp)

if args.output_dir:
    fp.close()
    fp = open(os.path.join(args.output_dir, "table4.txt"), "wt")
else:
    fp = sys.stdout

print("%\n% Table 4 in the original article\n%", file=fp)
if not args.latex:
    print("Featureset F1w(it) σF1w(it) ∆F1w(it) F1m(it) σF1m(it) "
          "F1w(cz) σF1w(cz) ∆F1w(cz) F1m(cz) σF1m(cz)", file=fp)
for k,v in cross.items():
    print("{:10}".format(k), end="", file=fp)
    for lang in ("it", "cz"):
        print("{}{:2.1f} {}{}{:2.2f} {}{:+2.1f} {}{:2.1f} {}{}{:2.1f}".format(
            sep, np.mean(v[lang]['w']),
            sep, pm, np.std(v[lang]['w']),
            sep, np.mean(v[lang]['w']) - 100*vr_tbl4[k][lang],
            sep, np.mean(v[lang]['m']),
            sep, pm, np.std(v[lang]['m'])), end=" ", file=fp)
    print(eol, file=fp)

if args.output_dir:
    fp.close()
